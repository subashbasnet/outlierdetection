package org.dfki.flink.stream;

import java.util.Comparator;

import org.apache.flink.api.java.tuple.Tuple3;
import org.dfki.flink.stream.GBOD.Grid;

public class GridCountComparator implements Comparator<Tuple3<Grid, Double, Long>> {

	/**
	 * 
	 */

	GridCountComparator() {
	}

	/**
	 * @return -1, 1
	 * @param o1
	 * @param o2
	 *            computes the tuple result in Ascending order comparing the f1
	 *            values
	 */
	@Override
	public int compare(Tuple3<Grid, Double, Long> o1, Tuple3<Grid, Double, Long> o2) {
		if (o1.f2 < o2.f2) {
			return -1;
		} else {
			return 1;
		}
	}
}
