package org.dfki.flink.stream;

import java.util.Comparator;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import org.dfki.flink.stream.StockAnalysis.Centroid;
import org.dfki.flink.stream.StockAnalysis.Point;

public class Tuple3Comparator implements Comparator<Tuple3<Centroid, Tuple2<String, Point>, Double>> {

	/**
	 * 
	 */

	Tuple3Comparator() {
	}

	/**
	 * @return -1, 1
	 * @param o1
	 * @param o2
	 *            computes the tuple result in Ascending order comparing the f2
	 *            values
	 */
	@Override
	public int compare(Tuple3<Centroid, Tuple2<String, Point>, Double> o1,
			Tuple3<Centroid, Tuple2<String, Point>, Double> o2) {
		if (o1.f2 < o2.f2) {
			return -1;
		} else if (o1.f2 > o2.f2) {
			return 1;
		} else {
			return 0;
		}
	}
}
