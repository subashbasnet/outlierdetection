package org.dfki.flink.stream;

import org.apache.flink.streaming.api.datastream.DataStream;

public class DataStreamStock {

	private DataStream<Stock> dStock; 
	
	public DataStreamStock(){}

	public DataStream<Stock> getdStock() {
		return dStock;
	}

	public void setdStock(DataStream<Stock> dStock) {
		this.dStock = dStock;
	}
}
