package org.dfki.flink.stream;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.dfki.flink.stream.GBOD.Grid;
import org.dfki.flink.stream.StockAnalysis.Centroid;
import org.dfki.flink.stream.StockAnalysis.Point;

public class BaseFunctions {

	public static double calcluateMedian(List<Tuple3<Centroid, Tuple2<String, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
		} else// for odd number of elements, '1' is deducted as index starts
				// from zero
			return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
	}

	public static double calcluateQ1(List<Tuple3<Centroid, Tuple2<String, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
			}
		}
	}

	public static double calcluateQ3(List<Tuple3<Centroid, Tuple2<String, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {// size added to the end to count from median
							// element
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size).f2
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size).f2;
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			// size+1 added to the end to count from after median element
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size + 1).f2
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size + 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size + 1).f2;
			}
		}
	}

	public static double calculateQ1ForDistance(List<Double> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1) + elementsWithDistance.get(size / 2 + 1 - 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1);
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1) + elementsWithDistance.get(size / 2 + 1 - 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1);
			}
		}
	}

	public static double calculateQ3ForDistance(List<Double> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {// size added to the end to count from median
							// element
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size)
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size);
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			// size+1 added to the end to count from after median element
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size + 1)
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size + 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size + 1);
			}
		}
	}

	public static double euclideanDistance(double pt1[], double pt2[]) {
		double distance = 0;
		for (int i = 0; i < pt2.length; i++) {
			distance += (pt1[i] - pt2[i]) * (pt1[i] - pt2[i]);
		}
		return Math.sqrt(distance);
	}

	public static double[] getLowerAndUpperBound(List<Double> distanceList) {
		double bounds[] = new double[2];
		double q1 = 0;
		double q3 = 0;
		if (distanceList.size() > 1) {
			q1 = calculateQ1ForDistance(distanceList);
			q3 = calculateQ3ForDistance(distanceList);
		}
		// find IQR
		double iqr = q3 - q1;
		// multiplying iqr by 1.5 to find bounds is standard practice
		bounds[0] = q1 - 1.5 * iqr;
		bounds[1] = q3 + 1.5 * iqr;
		return bounds;
	}

	/**
	 * Get normalized values, the mininum and maximum is received for each
	 * attribute of the row that means the total number of min and maxium will
	 * be 2*dimension.
	 */
	public static Double[] getNormalizedValues(Double[] minMax, Double x[]) {
		Double z[] = new Double[x.length];
		// Arrays.sort(x);//commented this line as I don't remember why I had
		// used it.
		// as when sorted the min-max doesn't get calcuated for the actual value
		int i = 0;
		int j = 0;
		for (double v : x) {
			if (v >= minMax[j] && v <= minMax[j + 1]) {
				z[i] = (double) (v - minMax[j]) / (double) (minMax[j + 1] - minMax[j]);
			} else {
				// the point is an outlier
			}
			if (z[i] != null) {
				if (Double.isNaN(z[i])) {
					z[i] = 0.0;
				}
			}
			i++;
			j += 2;// for next minmax is after 2numbers
		}
		return z;
	}

	// /**
	// * Get normalized values, the mininum and maximum is received for each
	// * attribute of the row that means the total number of min and maxium will
	// * be 2*dimension.
	// */
	// public static Double[] getNormalizedValues(Double[] minMax, Double x[]) {
	// Double z[] = new Double[x.length];
	// //Arrays.sort(x);
	// //boolean containsNaN = false;
	// int i = 0;
	// int j = 0;
	// for (double v : x) {
	// if (v >= minMax[j] && v <= minMax[j + 1]) {
	// z[i] = (double) (v - minMax[j]) / (double) (minMax[j + 1] - minMax[j]);
	// } else {
	// // the point is an outlier
	// }
	// if(Double.isNaN(z[i])){
	// z[i] = 0.0;
	// }
	// i++;
	// j += 2;//for next minmax is after 2numbers
	// }
	//// if (containsNaN) {
	//// Arrays.fill(z, 0.0);
	//// }
	// return z;
	// }

	/**
	 * Get stock symbols list from arguments
	 **/
	public static List<String> getSymbols(String args) {
		String stocks[] = args.split(",");
		List<String> stockSymbols = new ArrayList<>();
		for (String stock : stocks) {
			stockSymbols.add(stock.trim());
		}
		return stockSymbols;
	}

	/**
	 * Get normalized values
	 */
	public static Grid getRoundDownValues(Double x[], Integer parameter) {
		Integer z[] = new Integer[x.length];
		String id = "";
		for (int i = 0; i < x.length; i++) {
			if (x[i] == null) {
				x[i] = 0.0;
			}
			z[i] = (int) (x[i] * parameter);
			id += z[i].toString();
		}
		return new Grid(id, z);
	}

	/**
	 * Get arraylist for csv values 43 is the number of columns
	 **/
	// Utility which converts CSV to ArrayList using Split Operation
	public static AgataData CSVtoArray(String CSV) {
		AgataData agataData = new AgataData();
		if (CSV != null) {
			// -1 is used here to address the trailing spaces
			String[] splitData = CSV.split("\\s*,\\s*", -1);
			Double[] agata = new Double[splitData.length - 1];
			for (int i = 0; i < splitData.length; i++) {
				if (!(splitData[i] == null) && !(splitData[i].length() == 0)) {
					if (i == 0) {
						agataData.setId(splitData[i]);
					} else {
						agata[i - 1] = Double.parseDouble(splitData[i]);
					}
				} else {
					agata[i - 1] = 0.0;
				}
			}
			agataData.setData(agata);
		}
		return agataData;
	}

	// Utility which converts CSV to ArrayList using Split Operation
	public static AgataData CSVtoArrayNormal(String CSV) {
		AgataData agataData = new AgataData();
		if (CSV != null) {
			// -1 is used here to address the trailing spaces
			String[] splitData = CSV.split("\\s*,\\s*", -1);
			Double[] agata = new Double[splitData.length];
			for (int i = 0; i < splitData.length; i++) {
				if (!(splitData[i] == null) && !(splitData[i].length() == 0)) {
					agata[i] = Double.parseDouble(splitData[i]);
				} else {
					agata[i] = 0.0;
				}
			}
			agataData.setData(agata);
		}
		return agataData;
	}
}
