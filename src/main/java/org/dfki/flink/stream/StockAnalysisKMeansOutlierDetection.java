package org.dfki.flink.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.contrib.streaming.DataStreamUtils;
import org.apache.flink.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.IterativeStream.ConnectedIterativeStreams;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoFlatMapFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;
import org.apache.log4j.Logger;
import org.dfki.flink.stream.StockAnalysis.Centroid;
import org.dfki.flink.stream.StockAnalysis.CentroidAccumulator;
import org.dfki.flink.stream.StockAnalysis.CentroidAverager;
import org.dfki.flink.stream.StockAnalysis.ClusteredPoint;
import org.dfki.flink.stream.StockAnalysis.CountAppender;
import org.dfki.flink.stream.StockAnalysis.Point;
import org.dfki.flink.stream.StockAnalysis.TupleCentroidConverter;
import org.dfki.flink.stream.StockAnalysis.TupleClusteredPointConverter;
import org.dfki.flink.stream.StockAnalysis.calculateDistanceToCenter;
import org.dfki.flink.stream.StockAnalysis.getPoints;

//input symbol
//Eg: GOOG, FB
public class StockAnalysisKMeansOutlierDetection {

	final static Logger logger = Logger.getLogger(StockAnalysisKMeansOutlierDetection.class);

	public static void main(String[] args) throws Exception {
		logger.info("start execution");
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
		if(args.length==0){
			return;
		}
		DataStream<Stock> edits = see.addSource(new FetchStock(BaseFunctions.getSymbols(args[0])));

		@SuppressWarnings("serial")
		KeyedStream<Stock, String> keyedEdits = edits.keyBy(new KeySelector<Stock, String>() {
			@Override
			public String getKey(Stock event) {
				return event.getId();
			}
		});
		// edits.writeAsCsv("file:///home/softwares/flink-1.0.0/kmeans/streaming/result",
		// FileSystem.WriteMode.OVERWRITE);
		Double[] columns1 = new Double[5];
		@SuppressWarnings("serial")
		DataStream<Tuple2<String, Double[]>> newCentroidDataStream = keyedEdits.timeWindow(Time.seconds(5))
				.fold(new Tuple2<>("", columns1), new FoldFunction<Stock, Tuple2<String, Double[]>>() {
					@Override
					public Tuple2<String, Double[]> fold(Tuple2<String, Double[]> st, Stock value) {
						Double[] columns = new Double[5];// close,high,low,open,volume
						columns[0] = value.getClose();
						columns[1] = value.getHigh();
						columns[2] = value.getLow();
						columns[3] = value.getOpen();
						columns[4] = (double) value.getVolume();
						return (new Tuple2<String, Double[]>(value.getId(), columns));
					}
				});
		//newCentroidDataStream.print();
		columns1 = new Double[5];
		@SuppressWarnings("serial")
		DataStream<Tuple2<String, Double[]>> newDataStream = keyedEdits.timeWindow(Time.seconds(20))
				.fold(new Tuple2<>("", columns1), new FoldFunction<Stock, Tuple2<String, Double[]>>() {
					@Override
					public Tuple2<String, Double[]> fold(Tuple2<String, Double[]> st, Stock value) {
						Double[] columns = new Double[5];// close,high,low,open,volume
						columns[0] = value.getClose();
						columns[1] = value.getHigh();
						columns[2] = value.getLow();
						columns[3] = value.getOpen();
						columns[4] = (double) value.getVolume();
						return (new Tuple2<String, Double[]>(value.getId(), columns));
					}
				});
		//newDataStream.print();
		DataStream<Point> points = newDataStream.map(new getPoints());
		DataStream<Centroid> centroids = newCentroidDataStream.map(new TupleCentroidConverter());
		
		Iterator<Centroid> iter = DataStreamUtils.collect(centroids);
		Collection<Centroid> collectionCentroids = Lists.newArrayList(iter);
		
		DataStream<Centroid> newCentroids = points.map(new SelectNearestCenter(collectionCentroids))
				.map(new CountAppender()).keyBy(0).reduce(new CentroidAccumulator()).map(new CentroidAverager());
		
		//not used this connected iterative stream
//		 ConnectedIterativeStreams<Point, Centroid> loop =
//		 points.iterate().withFeedbackType(Centroid.class);
//		 DataStream<Centroid> newCentroids = loop.flatMap(new
//		 SelectNearestCenterForPoints(10))
//		 .map(new CountAppender()).keyBy(0).reduce(new
//		 CentroidAccumulator()).map(new CentroidAverager());
//		 DataStream<Centroid> finalCentroids =
//		 loop.closeWith(newCentroids.union(centroids).broadcast());
		
		//use finalcentroids below incase of connectedIterativeStreams otherwise use newCentroids	
		Iterator<Centroid> iter1 = DataStreamUtils.collect(newCentroids);
		Collection<Centroid> finalCentroidsCollection = Lists.newArrayList(iter1);
		
		DataStream<Tuple2<String, Point>> clusteredPoints = points
				// assign points to final clusters
				.map(new SelectNearestCenter(finalCentroidsCollection));
		DataStream<ClusteredPoint> newClusteredPoints = clusteredPoints.map(new TupleClusteredPointConverter());
//
//		//todo: try to use the same as before for finding centroids, then do as above. See the performance. 
//		//execute and wait if it never throws null in id and boolean,
//		//debug and check when can be the not null case
//		
		Iterator<Tuple2<String, Point>> iter2 = DataStreamUtils.collect(clusteredPoints);
		Collection<Tuple2<String, Point>> clusteredPointsCollection = Lists.newArrayList(iter2);
		
		
		DataStream<Tuple3<String, Point, Boolean>> distancePoints = newClusteredPoints
				.map(new calculateDistanceToCenter(finalCentroidsCollection, 
						clusteredPointsCollection));
		distancePoints.print();
		//distancePoints.writeAsCsv("file:///home/softwares/flink-1.0.0/kmeans/result", null, "\n", " ");
		// it's fucking printign mannnnn...bitch...ufff...
		// we can see the above centroids is not being used here below at all,
		// that means it cannot
		// be sent to datastream at all, but just we can do, se
		// but the problem is i am doing mean operation here, and
		// if I am not able to use this centroid values below it doesn't make
		// sense

		// todo: was trying to pass centroids and see, but I guess I have
		// already done that

		// ConnectedIterativeStreams<Point, ClusteredPoint> loop1 =
		// points.iterate().withFeedbackType(ClusteredPoint.class);
		// DataStream<ClusteredPoint> clusteredPoints = loop1.flatMap(new
		// SelectNearestCenterForPoints(10));
		// loop1.closeWith(finalCentroids.broadcast());
		// clusteredPoints.print();
		// todo: -1 set from coflatmap1 is being set,
		// that is timestamp
		// finalCentroids.print();
		// ConnectedIterativeStreams<Point, Cen> loop1 =
		// points.iterate().withFeedbackType
		// (PointWithCentroidId.class);
		// DataStream<PointWithCentroidId> clusteredPoints = loop1
		// // assign points to final clusters
		// .flatMap(new SelectNearestCenterForPoints(10));
		// DataStream<CentroidWithPoints> newCluteredPoints =
		// loop1.closeWith(clusteredPoints.broadcast());
		// // clusteredPoints.print();
		// DataStream<ClusteredPoint> newClusteredPoints =
		// clusteredPoints.map(new TupleClusteredPointConverter());
		// DataStream<Tuple3<String, Point, Boolean>> distancePoints =
		// newClusteredPoints
		// .map(new calculateDistanceToCenter());
		// distancePoints.print();
		see.execute();
	}

	/** Determines the closest cluster center for a data point. */
	@ForwardedFields("*->1")
	public static final class SelectNearestCenter extends RichMapFunction<Point, Tuple2<String, Point>> {
		private Collection<Centroid> centroids;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);

		}

		public SelectNearestCenter(Collection<Centroid> centroids) {
			this.centroids = centroids;
		}

		@Override
		public Tuple2<String, Point> map(Point p) throws Exception {

			double minDistance = Double.MAX_VALUE;
			String closestCentroidId = "-1";

			// check all cluster centers
			for (Centroid centroid : centroids) {
				// compute distance
				double distance = p.euclideanDistance(centroid);

				// update nearest cluster if necessary
				if (distance < minDistance) {
					minDistance = distance;
					closestCentroidId = centroid.id;
				}
			}

			// emit a new record with the center id and the data point.
			return new Tuple2<String, Point>(closestCentroidId, p);
		}
	}

	public static final class SelectNearestCenterForPoints
			implements CoFlatMapFunction<Point, Centroid, Tuple2<String, Point>> {
		private Centroid[] centroids;
		private int size = 0;
		private int count = 0;
		private boolean flag = true;
		private boolean collect = true;
		// private int centroidId = 0;

		// todo: try by using array instead of collection here
		public SelectNearestCenterForPoints(int size) {
			this.size = size;
		}

		@Override
		public void flatMap1(Point p, Collector<Tuple2<String, Point>> out) throws Exception {
			double minDistance = Double.MAX_VALUE;
			// centroidId++;
			String closestCentroidId = "-1";
			if (centroids != null) {
				collect = false;
				// let's assume minimum size 20 for now
				for (Centroid centroid : centroids) {
					// compute distance
					double distance = p.euclideanDistance(centroid);
					// update nearest cluster if necessary
					if (distance < minDistance) {
						minDistance = distance;
						closestCentroidId = centroid.id;
					}
				}
				// emit a new record with the center id and the data point.
				out.collect(new Tuple2<String, Point>(closestCentroidId, p));
			}
			if (collect) {
				// emit a new record with the center id and the data point.
				out.collect(new Tuple2<String, Point>(closestCentroidId, p));
			}
		}

		@Override
		public void flatMap2(Centroid value, Collector<Tuple2<String, Point>> out) throws Exception {
			if (flag) {
				centroids = new Centroid[size];
				flag = false;
			}
			if (count < size) {
				// System.out.println(value);
				boolean centroidExists = false;
				for (Centroid centroid : centroids) {
					if (centroid != null && value != null) {
						if (centroid.id.equals(value.id)) {
							centroidExists = true;
							break;
						}
					}
				}
				if (!centroidExists) {
					System.out.println(value);
					centroids[count] = value;
					count++;
				}
			}
		}
	}

	// todo: checking the size of centroids from above and not
	// looping until the size becomes same as above
	// check all cluster centers
	// /** Determines the closest cluster center for a data point. */
	// @ForwardedFields("*->1")
	// public static final class SelectNearestCenter implements
	// CoFlatMapFunction<Point, Centroid, Tuple2<String, Point>> {
	// private Centroid[] centroids;
	// private int size = 10;
	//
	// private int count = 0;
	// private boolean flag = true;
	// private boolean collect = true;
	// private int centroidId = 0;
	// private Collection<Centroid> iter;
	//
	// // todo: try by using array instead of collection here
	// public SelectNearestCenter(int size, Collection<Centroid> iter) {
	// this.size = size;
	// this.iter = iter;
	// for (Centroid c : this.iter) {
	// System.out.println(c);
	// }
	// }
	//
	// @Override
	// public void flatMap1(Point p, Collector<Tuple2<String, Point>> out)
	// throws Exception {
	// double minDistance = Double.MAX_VALUE;
	// centroidId++;
	// String closestCentroidId = centroidId + "";
	// if (centroids != null) {
	// collect = false;
	// // let's assume minimum size 20 for now
	// for (Centroid centroid : centroids) {
	// // compute distance
	// double distance = p.euclideanDistance(centroid);
	// // update nearest cluster if necessary
	// if (distance < minDistance) {
	// minDistance = distance;
	// closestCentroidId = centroid.id;
	// }
	// }
	// // emit a new record with the center id and the data point.
	// out.collect(new Tuple2<String, Point>(closestCentroidId, p));
	// }
	// if (collect) {
	// // emit a new record with the center id and the data point.
	// out.collect(new Tuple2<String, Point>(closestCentroidId, p));
	// }
	// }
	//
	// @Override
	// public void flatMap2(Centroid value, Collector<Tuple2<String, Point>>
	// out) throws Exception {
	// if (flag) {
	// centroids = new Centroid[size];
	// flag = false;
	// }
	// if (count < size) {
	// // System.out.println(value);
	// boolean centroidExists = false;
	// for (Centroid centroid : centroids) {
	// if (centroid != null && value != null) {
	// if (centroid.id.equals(value.id)) {
	// centroidExists = true;
	// break;
	// }
	// }
	// }
	// if (!centroidExists) {
	// centroids[count] = value;
	// count++;
	// }
	// }
	// }
	// }

	// /** Determines the distance of center for a data point. */
	// public static final class calculateDistanceToCenter
	// extends RichMapFunction<ClusteredPoint, Tuple3<String, Point, Boolean>> {
	// private Collection<Centroid> centroids;
	// private Collection<Tuple2<String, Point>> localClusteredPoints;
	//
	// /**
	// * Reads the centroid values from a broadcast variable into a
	// * collection.
	// */
	// @Override
	// public void open(Configuration parameters) throws Exception {
	// // todo: check like that of loop here, due to datastreamutils next
	// // operator error
	// Iterator<Centroid> iter = DataStreamUtils.collect(finalCentroids);
	// this.centroids = Lists.newArrayList(iter);
	// for (Centroid centroid : centroids) {
	// // System.out.println("centroid -------->" + centroid);
	// }
	// Iterator<Tuple2<String, Point>> iter1 =
	// DataStreamUtils.collect(clusteredPoints);
	// this.localClusteredPoints = Lists.newArrayList(iter1);
	// for (Tuple2<String, Point> clusteredPoint : localClusteredPoints) {
	// // System.out.println("clusteredPoint-->" + clusteredPoint);
	// }
	// }
	//
	// /**
	// * Takes clusteredPoint p, i.e. a single point as input. Perform
	// * operations to detect whether this point is an out-lier or not.
	// * Returns with a boolean value if this point is an out-lier or not.
	// * This method gets called parallel for each points until the completion
	// * of execution for all points. The result is parallel written to the
	// * file by each node cluster.
	// **/
	// @Override
	// public Tuple3<String, Point, Boolean> map(ClusteredPoint p) throws
	// Exception {
	// List<Tuple3<Centroid, Tuple2<String, Point>, Double>>
	// elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<String,
	// Point>, Double>>();
	// Tuple3<String, Point, Boolean> returnElement = new Tuple3<String, Point,
	// Boolean>();// true
	// elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<String,
	// Point>, Double>>();
	// Tuple3<Centroid, Tuple2<String, Point>, Double> newElement = new
	// Tuple3<Centroid, Tuple2<String, Point>, Double>();
	// Centroid centroid = new Centroid();
	// for (Tuple2<String, Point> e : localClusteredPoints) {
	// // compute distance
	// if (e.f0 == p.id) {// comparing centroid ids
	// newElement = new Tuple3<Centroid, Tuple2<String, Point>, Double>();
	// for (Centroid cent : centroids) {
	// if (e.f0 == cent.id) {
	// centroid = cent;
	// }
	// }
	// double distance = e.f1.euclideanDistance(centroid);
	// // System.out.println("Distance between " + e.f1 + " &" +
	// // centroid + " =" + distance);
	//
	// newElement.setFields(centroid, e, distance);
	// elementsWithDistance.add(newElement);
	// }
	// }
	// // sorting the distances in ascending order
	// // System.out.print("before sorting-->" + elementsWithDistance);
	// Collections.sort(elementsWithDistance, new Tuple3Comparator());
	// // System.out.print("after sorting-->" + elementsWithDistance);
	// // finding q1 and q3 for even number of element counts;
	// // double median = 0;
	// double q1 = 0;
	// double q3 = 0;
	// // only calcuate if size is geater than equal to 2
	// if (elementsWithDistance.size() >= 2) {
	// // median = BaseFunctions.calcluateMedian(elementsWithDistance);
	// q1 = BaseFunctions.calcluateQ1(elementsWithDistance);
	// q3 = BaseFunctions.calcluateQ3(elementsWithDistance);
	// }
	//
	// // find IQR
	// double iqr = q3 - q1;
	// // multiplying iqr by 1.5 to find bounds is standard practice
	// double lowerbound = q1 - 1.5 * iqr;
	// double upperbound = q3 + 1.5 * iqr;
	// // outlier
	// boolean elementSet = false;
	// for (Tuple3<Centroid, Tuple2<String, Point>, Double> elementWithDistance
	// : elementsWithDistance) {
	// returnElement = new Tuple3<String, Point, Boolean>();
	// if (Arrays.equals(p.pt, elementWithDistance.f1.f1.pt)) {// comparing
	// // values
	// // of points, to
	// // verify if it's
	// // the same element
	// if (elementWithDistance.f2 < lowerbound || elementWithDistance.f2 >
	// upperbound) {
	// // set as outlier
	// returnElement.setFields(elementWithDistance.f1.f0,
	// elementWithDistance.f1.f1, true);
	// } else {
	// returnElement.setFields(elementWithDistance.f1.f0,
	// elementWithDistance.f1.f1, false);
	// }
	// elementSet = true;
	// }
	// if (elementSet) {
	// break;
	// }
	// }
	// // emit a new record with the center id, the data point and distance
	// // to the center.
	// return new Tuple3<String, Point, Boolean>(returnElement.f0, p,
	// returnElement.f2);
	// }
	// }

	// static ConnectedStreams<Centroid, Centroid> loop;
	// private static ConnectedIterativeStreams<Centroid, Centroid> loop;
	// private static DataStream<Centroid> finalCentroids;
	// private static DataStream<Tuple2<String, Point>> clusteredPoints;
}
