package org.dfki.flink.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.contrib.streaming.DataStreamUtils;
import org.apache.flink.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.dfki.flink.stream.StockAnalysis.Point;
import org.dfki.flink.stream.StockAnalysis.TuplePointToDistance;
import org.dfki.flink.stream.StockAnalysis.getPoints;

public class StockAnalysisKDDTreeOutlierDetection {

	public static void main(String[] args) throws Exception {
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();

		DataStream<Stock> edits = see.addSource(new FetchStock(BaseFunctions.getSymbols(args[0])));

		@SuppressWarnings("serial")
		KeyedStream<Stock, String> keyedEdits = edits.keyBy(new KeySelector<Stock, String>() {
			@Override
			public String getKey(Stock event) {
				return event.getId();
			}
		});

		Double[] columns1 = new Double[5];
		@SuppressWarnings("serial")
		DataStream<Tuple2<String, Double[]>> newDataStream = keyedEdits.timeWindow(Time.seconds(5))
				.fold(new Tuple2<>("", columns1), new FoldFunction<Stock, Tuple2<String, Double[]>>() {
					@Override
					public Tuple2<String, Double[]> fold(Tuple2<String, Double[]> st, Stock value) {
						Double[] columns = new Double[5];// close,high,low,open,volume
						columns[0] = value.getClose();
						columns[1] = value.getHigh();
						columns[2] = value.getLow();
						columns[3] = value.getOpen();
						columns[4] = (double) value.getVolume();
						return (new Tuple2<String, Double[]>(value.getId(), columns));
					}
				});

		DataStream<Point> points = newDataStream.map(new getPoints());
		points.print();
		Iterator<Point> iteratorPoints = DataStreamUtils.collect(points);
		Collection<Point> collectionPoints = Lists.newArrayList(iteratorPoints);
		DataStream<Tuple2<Point, Double>> pointsWithDistance = points
				.map(new findDistanceToNearestPoint(collectionPoints, 5));
		pointsWithDistance.print();
		DataStream<Double> distance = pointsWithDistance.map(new TuplePointToDistance());
		Iterator<Double> distancePoints = DataStreamUtils.collect(distance);
		Collection<Double> collectionDistancePoints = Lists.newArrayList(distancePoints);
		List<Double> distanceList = new ArrayList<>();
		distanceList.addAll(collectionDistancePoints);
		Collections.sort(distanceList, new DistanceComparator());
		double boundary[] = new double[2];
		boundary = BaseFunctions.getLowerAndUpperBound(distanceList);
		DataStream<Tuple2<Point, Boolean>> finalPoints = pointsWithDistance.map(new findOutliers(boundary));
		finalPoints.print();
		see.execute();
	}

	/**
	 * Find the outliers using IQR method. All the distances are passed and the
	 * points with outlier distance are considered as outliers, i.e. if the
	 * point is relatively very far from it's nearest point then it should most
	 * likely be an outlier.
	 */
	public static final class findOutliers extends RichMapFunction<Tuple2<Point, Double>, Tuple2<Point, Boolean>> {

		private static final long serialVersionUID = 1L;
		private double[] boundary;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);

		}

		public findOutliers(double[] boundary) {
			this.boundary = boundary;
		}

		@Override
		public Tuple2<Point, Boolean> map(Tuple2<Point, Double> point) throws Exception {
			boolean outlier = false;
			if (point.f1 < boundary[0] || point.f1 > boundary[1]) {
				// set as outlier
				outlier = true;
			}
			return new Tuple2<Point, Boolean>(point.f0, outlier);
		}
	}

	/**
	 * Find distance of the point to its nearest Point
	 * 
	 */
	public static final class findDistanceToNearestPoint extends RichMapFunction<Point, Tuple2<Point, Double>> {

		/**
		 * 
		 */
		//private static final long serialVersionUID = 1L;
		private Collection<Point> points;
		//private List<Point> newPoints; // this collection of point is
										// without the own point
		// otherwise the KDDTree will say the nearest point is same.
		int dim = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);

		}

		public findDistanceToNearestPoint(Collection<Point> points, int dim) {
			this.points = points;
			this.dim = dim;
			//this.newPoints = new ArrayList<>();
		}

		@Override
		public Tuple2<Point, Double> map(Point point) throws Exception {
			List<Point> newPoints  = new ArrayList<>();
			for (Point p : points) {
				if (!p.equals(point)) {
					newPoints.add(p);
				}
			}
			// now call KDDTree to find the closest point
			KDTree kdt = new KDTree(newPoints.size(), dim);
			double x[] = new double[dim];
			for (Point p : newPoints) {
				x = ArrayUtils.toPrimitive(p.pt);
				kdt.add(x, dim);
			}
			double s[] = new double[dim];
			s = ArrayUtils.toPrimitive(point.pt);
			KDNode kdn = kdt.find_nearest(s, dim);
			x = kdn.x;
			return new Tuple2<Point, Double>(point, BaseFunctions.euclideanDistance(s, x));
		}
	}
}
