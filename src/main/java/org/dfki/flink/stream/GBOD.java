package org.dfki.flink.stream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.contrib.streaming.DataStreamUtils;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.dfki.flink.stream.GBOD.Grid;
import org.dfki.flink.stream.StockAnalysis.Point;
import org.dfki.flink.stream.StockAnalysis.PointNormalized;
import org.dfki.flink.stream.StockAnalysis.getPoints;

/**
 * final time window is taken 30 seconds, and total counts = 600 manually
 * checked with multiple samples as there is no way to count the stream data
 * points input args[]. args[0] = stock symbols seperated by commas, Eg: AAPL,
 * GOOG, FB args[1] = grid size, p, Eg: 5, i.e. how many number of grids you
 * want the axes to be divided into. 'm' the dimension, here in stock data is
 * taken as '5' due to 5 features, i.e. close, high, low, open, volume 'min-max'
 * values for the features to calculate the normalized values should be given,
 * here taken certain values manually, otherwise normalization program will
 * throw exception if less or greater than min, max values respectively.
 *Input Arguments: GOOG,AAPL,FB,MSFT,IBM,FOX,MRVL,SAP,BP 20
 **/
public class GBOD {

	public static void main(String[] args) throws Exception {
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStream<Stock> edits = see.addSource(new FetchStock(BaseFunctions.getSymbols(args[0])));

		@SuppressWarnings("serial")
		KeyedStream<Stock, String> keyedEdits = edits.keyBy(new KeySelector<Stock, String>() {
			@Override
			public String getKey(Stock event) {
				return event.getId();
			}
		});

		// since there is no way to count how many number of elements are there
		// in the stream as that of dataset
		// where there is count function to calculate the number of elements in
		// the dataset, but not in case of stream
		// so I checked how many inputs come arrive in the timewindow that I
		// set:
		// After I receive the stock data via yahoo finance api, i parse it to
		// to the required form as stock object
		// then I use the flink source context of dataset api to add this
		// achieved list of stock objects to the
		// flink as a stream source.
		// Eg: 20ms duration of thread sleep is between passing each stock
		// objects to the datastream as next
		// element in the stream. The time window of 3sec has been set to
		// collect the stock objects in the stream
		// for that duration. So this way the number of elements that I recieve
		// the particular time window would be
		// (3*1000)/20 = 150 elements. Now I perform operation on this 150
		// elements persent in the current instance of the
		// datastream and the loop continues after this stream finishes with the
		// next 150 elements and so on.
		// let's try for 4-d now, the volume of stock i've not taken here
		// because during
		// normalization along with the volume other vlaues become very very
		// small due to the
		// high value of volume in comparison to other attributes.
		Double[] columns1 = new Double[5];
		@SuppressWarnings("serial")
		DataStream<Tuple2<String, Double[]>> newDataStream = keyedEdits.timeWindow(Time.seconds(5))
				.fold(new Tuple2<>("", columns1), new FoldFunction<Stock, Tuple2<String, Double[]>>() {
					@Override
					public Tuple2<String, Double[]> fold(Tuple2<String, Double[]> st, Stock value) {
						Double[] columns = new Double[5];// close,high,low,open,volume
						columns[0] = value.getClose();
						columns[1] = value.getHigh();
						columns[2] = value.getLow();
						columns[3] = value.getOpen();
						columns[4] = (double) value.getVolume();
						return (new Tuple2<String, Double[]>(value.getId(), columns));
					}
				});

		DataStream<Point> points = newDataStream.map(new getPoints());
		// points.print();
		// here pass the maximum and minimum point
		// take this size from args I suppose, now it's 8
		Double[] minMax = { 50.0, 1000.0, 60.05, 1100.0, 40.0, 1000.0, 45.0, 1200.0, 0.0, 1000000.0 };
		DataStream<Point> newPoints = points.map(new PointNormalized(minMax));
		// newPoints.print();
		// let's assume grid size, p = 5 for now. so we'll have 5pow(2) = 25
		// no. of grids this integer array represents the gird coordinate the
		// point
		// belongs to for now p =5 is used directly inside the base function
		// later make it general
		DataStream<Tuple2<Point, Grid>> pointsWithGridCoordinates = newPoints
				.map(new RoundDownNewPoints(Integer.parseInt(args[1])));
		// pointsWithGridCoordinates.print();
		// 149 as written above is used as count here, need to find the count
		// method
		// found out the keyBy or groupBy was not possible to apply in the
		// array
		// of integers, so need a
		// id like thing in the grid
		// you cannot make a figure of grids for 3-d or 4-d and try to count
		// for eg: for p(size of grid) =2, and m(dimension) = 3, the no. of
		// grids = p^m = 2^3= 8, cannot
		// figure it pictorially in my brain, but can 2-d ofcourse.

		DataStream<Tuple3<Grid, Double, Long>> gridWithDensity = pointsWithGridCoordinates.map(new AddCountAppender())
				.keyBy(2).timeWindow(Time.seconds(10)).reduce(new GridPointsCount()).map(new RetrieveGridWithCount())
				.map(new CalculateGridDensity(200));
		// to find out total density
		// gridWithDensity.print();
		// DataStream<Tuple4<Grid, Double, Long, Double>>
		// gridWithIndividualAndTotalDensity = gridWithDensity
		// .map(new AddTotalIntialDensity()).keyBy(3);
		// .reduce(new TotalGridDensity())
		// .map(new TotalGridDensityMap());
		// gridWithIndividualAndTotalDensity.print();
		// datastreams are infinite so there is no sorting and count
		// functions
		// in case of datastream
		// Iterator<Tuple3<Grid, Double, Long>> iter =
		// DataStreamUtils.collect(gridWithDensity);
		// List<Tuple3<Grid, Double, Long>> gridWithDensityList =
		// Lists.newArrayList(iter);

		// we'll do mapping of gridWithDensity and every operation inside the
		// mapping.

		// List<String> uniqueGridIdList = new ArrayList<>();
		// for (Tuple3<Grid, Double, Long> grid : gridWithDensityList) {
		// if (!uniqueGridIdList.contains(grid.f0.id)) {
		// uniqueGridIdList.add(grid.f0.id);
		// }
		// }
		// System.out.println("unique id grid-->" + uniqueGridIdList);
		// Collections.sort(gridWithDensityList, new GridDensityComparator());
		// List<Tuple3<Grid, Double, Long>> filteredGridWithDensityList = new
		// ArrayList<>();
		// for (String gridId : uniqueGridIdList) {
		// long count = 0;
		// Tuple3<Grid, Double, Long> filteredGridWithDensity = new Tuple3<Grid,
		// Double, Long>();
		// for (Tuple3<Grid, Double, Long> grid : gridWithDensityList) {
		// if (gridId.equals(grid.f0.id)) {
		// if (count < grid.f2) {
		// count = grid.f2;
		// filteredGridWithDensity = grid;
		// }
		// }
		// }
		// filteredGridWithDensityList.add(filteredGridWithDensity);
		// }
		DataStream<Tuple4<Grid, Long, Boolean, Integer>> finalGrid = gridWithDensity.map(new FindOutlierGrid(200));
		 finalGrid.writeAsCsv("file:///home/subash/Desktop/gb",FileSystem.WriteMode.NO_OVERWRITE,
		 "\n", " ");
		//finalGrid.print();
		see.execute();
	}

	/**
	 * sums the density of each point and adds it to each element
	 */
	@ForwardedFields("0")
	public static final class TotalGridDensity implements ReduceFunction<Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> reduce(Tuple4<Grid, Double, Long, Double> val1,
				Tuple4<Grid, Double, Long, Double> val2) {
			return new Tuple4<Grid, Double, Long, Double>(val1.f0, val1.f1, val1.f2, val1.f1 + val2.f1);
		}
	}

	/**
	 * maps grid coordinate points
	 */
	public static final class TotalGridDensityMap
			implements MapFunction<Tuple4<Grid, Double, Long, Double>, Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> map(Tuple4<Grid, Double, Long, Double> t) throws Exception {
			return new Tuple4<Grid, Double, Long, Double>(t.f0, t.f1, t.f2, t.f3);
		}
	}

	/** Append grid total density as 1.0 initally. */
	@ForwardedFields("f0;f1")
	public static final class AddTotalIntialDensity
			implements MapFunction<Tuple3<Grid, Double, Long>, Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> map(Tuple3<Grid, Double, Long> t) {
			return new Tuple4<Grid, Double, Long, Double>(t.f0, t.f1, t.f2, 1D);// not
																				// sure
																				// about
																				// 1D
																				// here
																				// check??
		}
	}

	/**
	 * finding the outlier grids
	 **/
	public static final class FindOutlierGrid
			extends RichMapFunction<Tuple3<Grid, Double, Long>, Tuple4<Grid, Long, Boolean, Integer>> {

		int count = 0;
		int totalPoints = 0;
		int gbod_count_limit = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public FindOutlierGrid(int totalPoints) {
			this.totalPoints = totalPoints;
			this.count = 0;
		}

		// do with density i.e. f0, then show how the results came then do with
		// count i.e. f1, then
		// describe if the results is more accurate..now explain that you were
		// not able to use the GBOD
		// algorithm of cumulative addition, becuase you needed to collect the
		// list. Creating a list from the
		// datastream i.e. infinite data is to make it finite which is not
		// plausible or even if it's created
		// using DataStreamUitls it'll create the error of double execution and
		// so on, cannot guarantee the streaming
		// operations will work always.

		// total density divided by total points in average density, then pass
		// this density and find out outliers, but while programming found out
		// that it couldn't
		// be done due to the fact that there exists no counting possibility of
		// number of
		// non empty grids as it changes each time, so taking time window to
		// find out manually
		// cannot be done here. Hence only did outlier detection via counting
		// method
		@Override
		public Tuple4<Grid, Long, Boolean, Integer> map(Tuple3<Grid, Double, Long> gd) throws Exception {
			// Collections.sort(gridWithDensityList, new
			// GridDensityComparator());
			// here limit = (total grid density/total number of grids(p^m))
			double limit = (totalPoints) * 0.05;// GBOD considers 5% of
												// the element to be
												// outliers but
			// we couldn't implement that, so we implemented grids with density
			// or grid less than 5% of the average density
			// or total elements count is considered as outlier grids, the
			// result will be compared.
			if (gbod_count_limit > 200) {// the size of the elements taken
											// currently
				gbod_count_limit = 0;
				count = 0;
			}
			count++;
			gbod_count_limit += gd.f2;
			if (gd.f2 < limit) {
				if (count < 4) {// taking 4 number of elements
					return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, true, count);
				} else {
					return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, false, count);
				}
			} else {
				return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, false, count);
			}
		}
	}

	/**
	 * finding density of each grid
	 **/
	public static final class CalculateGridDensity
			extends RichMapFunction<Tuple2<Grid, Long>, Tuple3<Grid, Double, Long>> {

		int totalPoints = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public CalculateGridDensity(int totalPoints) {
			this.totalPoints = totalPoints;
		}

		@Override
		public Tuple3<Grid, Double, Long> map(Tuple2<Grid, Long> gd) throws Exception {

			return new Tuple3<Grid, Double, Long>(gd.f0, (double) gd.f1 / (double) totalPoints, gd.f1);
		}
	}

	/**
	 * Grid based
	 */
	public static final class RetrieveGridWithCount
			implements MapFunction<Tuple4<Point, Grid, String, Long>, Tuple2<Grid, Long>> {

		@Override
		public Tuple2<Grid, Long> map(Tuple4<Point, Grid, String, Long> t) throws Exception {
			return new Tuple2<Grid, Long>(t.f1, t.f3);
		}
	}

	/**
	 * counts the number of points in a grid
	 */
	@ForwardedFields("0")
	public static final class GridPointsCount implements ReduceFunction<Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> reduce(Tuple4<Point, Grid, String, Long> val1,
				Tuple4<Point, Grid, String, Long> val2) {
			return new Tuple4<Point, Grid, String, Long>(val1.f0, val1.f1, val1.f2, val1.f3 + val2.f3);
		}
	}

	/** Appends a count variable to the tuple. */
	@ForwardedFields("f0;f1")
	public static final class AddCountAppender
			implements MapFunction<Tuple2<Point, Grid>, Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> map(Tuple2<Point, Grid> t) {
			return new Tuple4<Point, Grid, String, Long>(t.f0, t.f1, t.f1.id, 1L);
		}
	}

	/**
	 * A multi-dimensional grid.
	 */
	public static class Grid implements Serializable {

		public String id;

		public Integer gd[];

		public Grid() {
		}

		public Grid(String id, Integer gd[]) {
			this.id = id;
			this.gd = gd;
		}

		public Grid add(Grid other) {
			for (int i = 0; i < other.gd.length; i++) {
				gd[i] += other.gd[i];
			}
			return this;
		}

		public Grid div(Integer val) {
			for (int i = 0; i < gd.length; i++) {
				gd[i] /= val;
			}
			return this;
		}

		public double euclideanDistance(Grid other) {
			double distance = 0;
			for (int i = 0; i < other.gd.length; i++) {
				distance += (gd[i] - other.gd[i]) * (gd[i] - other.gd[i]);
			}
			return Math.sqrt(distance);
		}

		public void clear() {
			Arrays.fill(gd, 0);
		}

		@Override
		public String toString() {
			String distance = "";
			for (int i = 0; i < gd.length; i++) {
				distance = distance + " " + gd[i];
			}
			return distance;
		}
	}

	/**
	 * now let's multiply each value by p, the atrributes value will range from
	 * 0 to p. the return will be integer values so it'll correspond to the
	 * coordinate of the grid. i.e. the value says which grid the point belongs
	 * to.
	 **/
	public static final class RoundDownNewPoints extends RichMapFunction<Point, Tuple2<Point, Grid>> {

		int parameter = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public RoundDownNewPoints(int parameter) {
			this.parameter = parameter;
		}

		@Override
		public Tuple2<Point, Grid> map(Point pt) throws Exception {

			return new Tuple2<Point, Grid>(pt, BaseFunctions.getRoundDownValues(pt.pt,parameter));
		}
	}
}
