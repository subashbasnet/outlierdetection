package org.dfki.flink.stream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.contrib.streaming.DataStreamUtils;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.hadoop.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.IterativeStream.ConnectedIterativeStreams;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.dfki.flink.stream.StockAnalysis.Centroid;

//todo: the issue is there is no first() function as of dataSet to get required no. of clustered centroid points
//so now I will be writing two files, one with all data, and one with number of centroids data, getting no. of centroids 
//from user is better 
/**
 * Takes stock data, stock centroid data, output result path as inputs. Input
 * format: <stock-data-path> <centroid-data-path> <output-path> Eg:
 * file:///home/subash/Desktop/test.csv file:///home/subash/Desktop/centroid.csv
 * file:///home/subash/Desktop/result
 * 
 * 
 */
public class StockAnalysis {

	// private static int rowsCount = 0;
	public static void main(String[] args) throws Exception {
		if (!parseParameters(args)) {
			return;
		}
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
		while (true) {
			DataStream<String> dataStream = getCsvDataStream(see);
			DataStream<String> centroidDataStream = getCentroidCsvDataStream(see);
			DataStream<Stock> edits = dataStream.flatMap(new StockTokenizer());
			DataStream<Stock> editsCentroid = centroidDataStream.flatMap(new StockTokenizer());
			DataStream<Tuple2<String, Double[]>> newDataStream = edits.flatMap(new Tokenizer());
			DataStream<Tuple2<String, Double[]>> newCentroidDataStream = editsCentroid.flatMap(new Tokenizer());
			DataStream<Point> points = newDataStream.map(new getPoints());
			DataStream<Centroid> centroids = newCentroidDataStream.map(new TupleCentroidConverter());
			loop = centroids.iterate(10).withFeedbackType(Centroid.class);
			DataStream<Centroid> newCentroids = points.map(new SelectNearestCenter()).map(new CountAppender()).keyBy(0)
					.reduce(new CentroidAccumulator()).map(new CentroidAverager());
			finalCentroids = newCentroids;
			// useFinalCentroids = true;
			clusteredPoints = points
					// assign points to final clusters
					.map(new SelectNearestCenter());
			DataStream<ClusteredPoint> newClusteredPoints = clusteredPoints.map(new TupleClusteredPointConverter());
			// DataStream<Tuple3<String, Point, Boolean>> distancePoints =
			// newClusteredPoints
			// .map(new calculateDistanceToCenter());
			if (fileOutput) {
				System.out.println("----------before print------------");
				// distancePoints.print();
				System.out.println("----------after print------------");
				// distancePoints.writeAsText(outputPath,
				// FileSystem.WriteMode.OVERWRITE);
				// since file sinks are lazy, we trigger the execution
				// explicitly

				see.execute("KMeans Example");
			} else {
				// distancePoints.print();
			}
			// todo: not working, unable to write csv but it writes
			// text, no
			// answer from flink, as they might
			// have felt the question really lame, but the issue
			// persists in my
			// case.
			// @SuppressWarnings("serial")
			// KeyedStream<Stock, String> keyedEdits = edits.keyBy(new
			// KeySelector<Stock, String>() {
			// @Override
			// public String getKey(Stock event) {
			// return event.getId();
			// }
			// });

			// @SuppressWarnings("serial")
			// DataStream<Tuple2<Double, Double>> result =
			// keyedEdits.timeWindow(Time.seconds(30))
			// .fold(new Tuple2<>(0.0d, 0.0d), new FoldFunction<Stock,
			// Tuple2<Double, Double>>() {
			// @Override
			// public Tuple2<Double, Double> fold(Tuple2<Double, Double> acc,
			// Stock event) {
			// acc.f0 = event.getClose();
			// acc.f1 = event.getOpen();
			// // System.out.println("account-->" + acc);
			// return acc;
			// }
			// });
			// result.print();
			// result.writeAsText("file:///home/softwares/flink-1.0.0/kmeans/streaming/result",
			// FileSystem.WriteMode.OVERWRITE);

			// distancePoints.map(new MapFunction<Tuple3<String, Point,
			// Boolean>, String>() {
			// @Override
			// public String map(Tuple3<String, Point, Boolean> tuple) {
			// return tuple.toString();
			// }
			// }).addSink(new FlinkKafkaProducer08<>("localhost:9092",
			// "wiki-result", new SimpleStringSchema()));

			// see.execute();
			// todo: this thread sleep makes whole thread sleep but I need sleep
			// after current all process ends
			// once i saw the data being written to text file, never after that
			// gain :D
			// Thread.sleep(6000);
			System.out.println("--------------------------------------------------------");
		}

	}

	/**
	 * Implements the string tokenizer that splits sentences into words as a
	 * user-defined FlatMapFunction. The function takes a line (String) and
	 * splits it into multiple pairs in the form of "(word,1)" (
	 * {@code Tuple2<String, Integer>}).
	 */
	public static final class Tokenizer implements FlatMapFunction<Stock, Tuple2<String, Double[]>> {

		@Override
		public void flatMap(Stock value, Collector<Tuple2<String, Double[]>> out) {
			// normalize and split the line

			Double[] columns = new Double[5];// close,high,low,open,volume
			columns[0] = value.getClose();
			columns[1] = value.getHigh();
			columns[2] = value.getLow();
			columns[3] = value.getOpen();
			columns[4] = (double) value.getVolume();

			out.collect(new Tuple2<String, Double[]>(value.getId(), columns));
		}
	}

	/**
	 * Implements the string tokenizer that splits sentences into words as a
	 * user-defined FlatMapFunction. The function takes a line (String) and
	 * splits it into multiple pairs in the form of "(word,1)" (
	 * {@code Tuple2<String, Integer>}).
	 */
	@SuppressWarnings("serial")
	public static final class StockTokenizer implements FlatMapFunction<String, Stock> {

		@Override
		public void flatMap(String value, Collector<Stock> out) {
			// normalize and split the line
			String[] tokens = value.toLowerCase().split("\\s*,\\s*");
			Stock stock = new Stock();
			for (int i = 0; i < tokens.length; i++) {
				if (!tokens[i].equals("")) {
					if (NumberUtils.isNumber(tokens[i])) {// check if it's a
															// number
						switch (i) {
						case 1:
							stock.setClose(Double.parseDouble(tokens[i]));
							break;
						case 2:
							stock.setHigh(Double.parseDouble(tokens[i]));
							break;
						case 3:
							stock.setLow(Double.parseDouble(tokens[i]));
							break;
						case 4:
							stock.setOpen(Double.parseDouble(tokens[i]));
							break;
						case 5:
							stock.setVolume(Integer.parseInt(tokens[i]));
							break;
						}
					} else {// it's not a number now set it as id
						stock.setId(tokens[i]);
					}
				}
			}
			out.collect(stock);
		}
	}

	// *************************************************************************
	// DATA TYPES
	// *************************************************************************

	public static class CentroidWithPoints {
		private Point pt[];
		private Centroid centroid;

		public CentroidWithPoints(Centroid centroid, Point pt[]) {
			this.pt = pt;
			this.centroid = centroid;
		}

		public Point[] getPt() {
			return pt;
		}

		public void setPt(Point[] pt) {
			this.pt = pt;
		}

		public Centroid getCentroid() {
			return centroid;
		}

		public void setCentroid(Centroid centroid) {
			this.centroid = centroid;
		}
	}

	public static class ClusteredPoint extends Point {

		public String id;

		public ClusteredPoint() {
		}

		public ClusteredPoint(String id, Point p) {
			super(p.pt);
			this.id = id;
		}

		public ClusteredPoint(String id, Double pt[]) {
			super(pt);
			this.id = id;
		}

		@Override
		public String toString() {
			return id + " " + super.toString();
		}
	}

	/**
	 * A multi-dimensional point.
	 */
	public static class Point implements Serializable {

		public Double pt[];

		public Point() {
		}

		public Point(Double pt[]) {
			this.pt = pt;
		}

		public Point add(Point other) {
			for (int i = 0; i < other.pt.length; i++) {
				pt[i] += other.pt[i];
			}
			return this;
		}

		public Point div(long val) {
			for (int i = 0; i < pt.length; i++) {
				pt[i] /= val;
			}
			return this;
		}

		public double euclideanDistance(Point other) {
			double distance = 0;
			for (int i = 0; i < other.pt.length; i++) {
				distance += (pt[i] - other.pt[i]) * (pt[i] - other.pt[i]);
			}
			return Math.sqrt(distance);
		}

		public void clear() {
			Arrays.fill(pt, 0);
		}

		@Override
		public String toString() {
			String distance = "";
			for (int i = 0; i < pt.length; i++) {
				distance = distance + " " + pt[i];
			}
			return distance;
		}
	}

	/**
	 * A simple two-dimensional centroid, basically a point with an ID.
	 */
	public static class Centroid extends Point {

		public String id;

		public Centroid() {
		}

		public Centroid(String id, Double pt[]) {
			super(pt);
			this.id = id;
		}

		public Centroid(String id, Point p) {
			super(p.pt);
			this.id = id;
		}

		@Override
		public String toString() {
			return id + " " + super.toString();
		}
	}

	/** Determines the distance of center for a data point. */
	public static final class calculateDistanceToCenter
			extends RichMapFunction<ClusteredPoint, Tuple3<String, Point, Boolean>> {
		private Collection<Centroid> centroids;
		private Collection<Tuple2<String, Point>> localClusteredPoints;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);

		}

		public calculateDistanceToCenter(Collection<Centroid> centroids,
				Collection<Tuple2<String, Point>> localClusteredPoints) {
			this.centroids = centroids;
			this.localClusteredPoints = localClusteredPoints;
		}

		/**
		 * Takes clusteredPoint p, i.e. a single point as input. Perform
		 * operations to detect whether this point is an out-lier or not.
		 * Returns with a boolean value if this point is an out-lier or not.
		 * This method gets called parallel for each points until the completion
		 * of execution for all points. The result is parallel written to the
		 * file by each node cluster.
		 **/
		@Override
		public Tuple3<String, Point, Boolean> map(ClusteredPoint p) throws Exception {
			List<Tuple3<Centroid, Tuple2<String, Point>, Double>> elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<String, Point>, Double>>();
			Tuple3<String, Point, Boolean> returnElement = new Tuple3<String, Point, Boolean>();// true
			elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<String, Point>, Double>>();
			Tuple3<Centroid, Tuple2<String, Point>, Double> newElement = new Tuple3<Centroid, Tuple2<String, Point>, Double>();
			Centroid centroid = new Centroid();

			// need to check if the e.f0 is equal to any of p.id, i found none,
			// if not equal at all, then need to check regarding id
			// then how about sending very few data byself and using them as
			// stream and check if it works fine
			for (Tuple2<String, Point> e : localClusteredPoints) {
				// compute distance
				if (e.f0.equalsIgnoreCase(p.id)) {// comparing centroid ids
					newElement = new Tuple3<Centroid, Tuple2<String, Point>, Double>();
					for (Centroid cent : centroids) {
						if (e.f0.equalsIgnoreCase(cent.id)) {
							centroid = cent;
						}
					}
					double distance = e.f1.euclideanDistance(centroid);
					// System.out.println("Distance between " + e.f1 + " &" +
					// centroid + " =" + distance);

					newElement.setFields(centroid, e, distance);
					elementsWithDistance.add(newElement);
				}
			}
			// sorting the distances in ascending order
			// System.out.print("before sorting-->" + elementsWithDistance);
			Collections.sort(elementsWithDistance, new Tuple3Comparator());
			// System.out.print("after sorting-->" + elementsWithDistance);
			// finding q1 and q3 for even number of element counts;
			// double median = 0;
			double q1 = 0;
			double q3 = 0;
			// only calcuate if size is geater than equal to 2
			if (elementsWithDistance.size() > 1) {
				// median = BaseFunctions.calcluateMedian(elementsWithDistance);
				q1 = BaseFunctions.calcluateQ1(elementsWithDistance);
				q3 = BaseFunctions.calcluateQ3(elementsWithDistance);
			}

			// find IQR
			double iqr = q3 - q1;
			// multiplying iqr by 1.5 to find bounds is standard practice
			double lowerbound = q1 - 1.5 * iqr;
			double upperbound = q3 + 1.5 * iqr;
			// outlier
			boolean elementSet = false;
			for (Tuple3<Centroid, Tuple2<String, Point>, Double> elementWithDistance : elementsWithDistance) {
				returnElement = new Tuple3<String, Point, Boolean>();
				if (Arrays.equals(p.pt, elementWithDistance.f1.f1.pt)) {// comparing
																		// values
					// of points, to
					// verify if it's
					// the same element
					if (elementWithDistance.f2 < lowerbound || elementWithDistance.f2 > upperbound) {
						// set as outlier
						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, true);
					} else {
						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, false);
					}
					elementSet = true;
				}
				if (elementSet) {
					break;
				}
			}
			// emit a new record with the center id, the data point and distance
			// to the center.
			return new Tuple3<String, Point, Boolean>(returnElement.f0, p, returnElement.f2);
		}
	}

	/** Converts a {@code Tuple3<Integer, Double,Double>} into a Centroid. */
	// @ForwardedFields("0->id; 1->x; 2->y")
	public static final class TupleClusteredPointConverter
			implements MapFunction<Tuple2<String, Point>, ClusteredPoint> {

		@Override
		public ClusteredPoint map(Tuple2<String, Point> t) throws Exception {
			return new ClusteredPoint(t.f0, t.f1);
		}
	}

	/**
	 * Converts points with distance to distance only
	 */
	public static final class TuplePointToDistance implements MapFunction<Tuple2<Point, Double>, Double> {

		@Override
		public Double map(Tuple2<Point, Double> t) throws Exception {
			return new Double(t.f1);
		}
	}

	/**
	 * Normalizes points with values between 0 and 1
	 */
	public static final class PointNormalized extends RichMapFunction<Point, Point> {
		
		Double minMax[] = new Double[5];

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public PointNormalized(Double[] minMax) {
			this.minMax = minMax;
		}
		
		@Override
		public Point map(Point pt) throws Exception {
			return new Point(BaseFunctions.getNormalizedValues(minMax, pt.pt));
		}
	}

	/** Determines the closest cluster center for a data point. */
	@ForwardedFields("*->1")
	public static final class SelectNearestCenter extends RichMapFunction<Point, Tuple2<String, Point>> {
		private Collection<Centroid> centroids;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			// todo: check if this works not sure, might throw cast exceptions
			System.out.println("----------printing loop----------");
			Iterator<Centroid> iter = DataStreamUtils.collect(loop.getFirstInput());
			this.centroids = Lists.newArrayList(iter);
			for (Centroid centroid : centroids) {
				System.out.println("centroid id-->" + centroid.id);
			}
			// if (!useFinalCentroids) {
			// Iterator<Centroid> iter = DataStreamUtils.collect(loop);
			// this.centroids = Lists.newArrayList(iter);
			// } else {
			// Iterator<Centroid> iter =
			// DataStreamUtils.collect(finalCentroids);
			// this.centroids = Lists.newArrayList(iter);
			// }

		}

		@Override
		public Tuple2<String, Point> map(Point p) throws Exception {

			double minDistance = Double.MAX_VALUE;
			String closestCentroidId = "-1";

			// check all cluster centers
			for (Centroid centroid : centroids) {
				// compute distance
				double distance = p.euclideanDistance(centroid);

				// update nearest cluster if necessary
				if (distance < minDistance) {
					minDistance = distance;
					closestCentroidId = centroid.id;
				}
			}

			// emit a new record with the center id and the data point.
			return new Tuple2<String, Point>(closestCentroidId, p);
		}
	}

	/** Converts a {@code Tuple3<Integer, Double,Double>} into a Centroid. */
	@ForwardedFields("0->id;")
	public static final class TupleCentroidConverter implements MapFunction<Tuple2<String, Double[]>, Centroid> {

		@Override
		public Centroid map(Tuple2<String, Double[]> ct) throws Exception {
			return new Centroid(ct.f0, ct.f1);
		}
	}

	/** converts Tuple2<Integer, double[]> to Point */
	// @ForwardedFields("0->id")
	public static final class getPoints implements MapFunction<Tuple2<String, Double[]>, Point> {

		@Override
		public Point map(Tuple2<String, Double[]> value) {
			return new Point(value.f1);
		}
	}

	public static final class getResult implements MapFunction<Stock, Stock> {

		@Override
		public Stock map(Stock value) {
			return new Stock();
		}
	}

	/**
	 * Read stock Csv data from input path
	 */
	private static DataStream<String> getCsvDataStream(StreamExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(inputCsvPath);
		} else {
			// return null
			return null;
		}
	}

	/** Appends a count variable to the tuple. */
	@ForwardedFields("f0;f1")
	public static final class CountAppender implements MapFunction<Tuple2<String, Point>, Tuple3<String, Point, Long>> {

		@Override
		public Tuple3<String, Point, Long> map(Tuple2<String, Point> t) {
			return new Tuple3<String, Point, Long>(t.f0, t.f1, 1L);
		}
	}

	/** Sums and counts point coordinates. */
	@ForwardedFields("0")
	public static final class CentroidAccumulator implements ReduceFunction<Tuple3<String, Point, Long>> {

		@Override
		public Tuple3<String, Point, Long> reduce(Tuple3<String, Point, Long> val1, Tuple3<String, Point, Long> val2) {
			return new Tuple3<String, Point, Long>(val1.f0, val1.f1.add(val2.f1), val1.f2 + val2.f2);
		}
	}

	/** Computes new centroid from coordinate sum and count of points. */
	@ForwardedFields("0->id")
	public static final class CentroidAverager implements MapFunction<Tuple3<String, Point, Long>, Centroid> {

		@Override
		public Centroid map(Tuple3<String, Point, Long> value) {
			return new Centroid(value.f0, value.f1.div(value.f2));
		}
	}

	/**
	 * Read stock Centroid Csv data from input path
	 */
	private static DataStream<String> getCentroidCsvDataStream(StreamExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(inputCentroidCsvPath);
		} else {
			// return null
			return null;
		}
	}

	private static boolean parseParameters(String[] programArguments) {

		if (programArguments.length > 0) {
			// parse input arguments
			fileOutput = true;
			if (programArguments.length == 3) {
				inputCsvPath = programArguments[0];
				inputCentroidCsvPath = programArguments[1];
				outputPath = programArguments[2];
			} else {
				System.err.println("Usage: KMeans <points path> <centers path> <result path> <num iterations>");
				return false;
			}
		} else {
			System.out.println("Executing K-Means example with default parameters and built-in default data.");
			System.out.println("  Provide parameters to read input data from files.");
			System.out.println("  See the documentation for the correct format of input files.");
			System.out.println("  We provide a data generator to create synthetic input files for this program.");
			System.out.println("  Usage: KMeans <points path> <centers path> <result path> <num iterations>");
		}
		return true;
	}

	private static boolean fileOutput = false;
	private static String inputCsvPath = null;
	private static String inputCentroidCsvPath = null;
	private static String outputPath = null;
	private static ConnectedIterativeStreams<Centroid, Centroid> loop;
	private static DataStream<Centroid> finalCentroids;
	// private static boolean useFinalCentroids = false;
	private static DataStream<Tuple2<String, Point>> clusteredPoints;
}
