package org.dfki.flink.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext;
import org.apache.flink.util.Collector;
import org.json.JSONArray;
import org.json.JSONObject;

public class FetchStock extends RichSourceFunction<Stock> {

	private volatile boolean isRunning = true;
	// private final StreamExecutionEnvironment env;
	// private final boolean fileOutput;
	// private final String inputCsvPath;
	private List<String> symbol;
	// private static SourceContext<Stock> srcContext;
	private static BlockingQueue<Stock> queueStock;
	private static Stock st;
	public boolean running = true;

	public FetchStock(List<String> symbol) {
		// this.env = StreamExecutionEnvironment.getExecutionEnvironment();
		// this.fileOutput = fileOutput;
		// this.inputCsvPath = inputCsvPath;
		this.symbol = symbol;
	}

	/*
	 * Returns a Stock Object that contains info about a specified stock.
	 * 
	 * @param symbol the company's stock symbol
	 * 
	 * @return a stock object containing info about the company's stock
	 * 
	 * @see Stock
	 */
	static List<Stock> getStock(List<String> symbol) {
		List<Stock> stockList = new ArrayList<>();
		try {

			// Problem: the volume of google is not accurate as said by many and
			// I checked with other API's also.
			// http://www.google.com/finance/getprices?q=FB&x=NASD&i=120&sessions=ext_hours&p=3d&f=d,c,v,o,h,l
			// http://chartapi.finance.yahoo.com/instrument/1.0/FB/chartdata;type=quote;range=1d/csv
			// Retrieve CSV File
			// URL yahoo = new URL("http://finance.yahoo.com/d/quotes.csv?s="+
			// symbol + "&f=l1vr2ejkghm3j3nc4s7pox");
			for (String sym : symbol) {
				URL yahoo = new URL("http://chartapi.finance.yahoo.com/instrument/1.1/" + sym
						+ "/chartdata;type=quote;range=1d/json");
				URLConnection connection = yahoo.openConnection();
				InputStreamReader is = new InputStreamReader(connection.getInputStream());
				BufferedReader br = new BufferedReader(is);
				StringBuilder responseStrBuilder = new StringBuilder();

				String inputStr;
				while ((inputStr = br.readLine()) != null)
					responseStrBuilder.append(inputStr);
				// 30 is the length of "finance_charts_json_callback(", needed
				// to be
				// removed to convert
				// to proper json format
				String jsonResponse = responseStrBuilder.substring(30, responseStrBuilder.length() - 1);
				JSONObject jsonObject = new JSONObject(jsonResponse);
				JSONArray jsArray = jsonObject.getJSONArray("series");
				Stock stock = new Stock();
				for (int i = 0; i < jsArray.length(); ++i) {
					JSONObject js = jsArray.getJSONObject(i);
					stock = new Stock();
					stock.setTimeStamp(js.getInt("Timestamp"));
					java.util.Date time = new java.util.Date((long) stock.getTimeStamp() * 1000);
					stock.setId(time + "");
					stock.setClose(js.getDouble("close"));
					stock.setHigh(js.getDouble("high"));
					stock.setLow(js.getDouble("low"));
					stock.setOpen(js.getDouble("open"));
					stock.setVolume(js.getInt("volume"));
					// if(i==2){//testing to set outlier
					// stock.setClose(1500);
					// stock.setHigh(1700);
					// stock.setLow(2);
					// stock.setOpen(1200);
					// stock.setVolume(1);
					// }
					// if(i==3){//testing to set outlier
					// stock.setClose(15000);
					// stock.setHigh(17000);
					// stock.setLow(23000);
					// stock.setOpen(12000);
					// stock.setVolume(100000000);
					// }
					stockList.add(stock);
				}
			}
		} catch (IOException e) {
			Logger log = Logger.getLogger(FetchStock.class.getName());
			log.log(Level.SEVERE, e.toString(), e);
			return null;
		}
		// The list is shuffled now to make the input not continuous from a
		// particular stock, but mix of stocks
		Collections.shuffle(stockList);
		return stockList;

	}

	// @Override
	// public void run(SourceContext<Stock> ctx) throws Exception {
	// while (isRunning) {
	// // Stock st = new Stock();
	// Thread.sleep(5000);
	// StreamExecutionEnvironment env =
	// StreamExecutionEnvironment.getExecutionEnvironment();
	// DataStream<String> dataStream = getCsvDataStream(env, fileOutput,
	// inputCsvPath);
	// System.out.println("hello before");
	// dataStream.print();
	// System.out.println("hello after");
	// DataStream<Stock> edits = dataStream.flatMap(new Tokenizer());
	// // DataStreamStock dStock = new DataStreamStock();
	// // dStock.setdStock(edits);
	// if (edits != null) {
	// ctx.collect(st);
	// }
	// }

	// }

	// for GBOD
	// @Override
	// public void run(SourceContext<Stock> ctx) throws Exception {
	// List<Stock> stockList = getStock(symbol);// query stocklist
	// while (this.running) {
	// for (Stock stock : stockList) {
	// ctx.collect(stock);
	// Thread.sleep(50);// sleep for 10000ms
	// }
	// stockList = getStock(symbol);// for getting new refreshed data
	// }
	// }

	// for KMeans
	@Override
	public void run(SourceContext<Stock> ctx) throws Exception {
		List<Stock> stockList = getStock(symbol);// query stocklist
//		 int size = 300;
//		 if(stockList.size()<size){
//		 size = stockList.size();
//		 }
//		 for (int i=0; i<size;i++) {
//		 ctx.collect(stockList.get(i));
//		 Thread.sleep(20);// sleep for 500ms
//		 //
//		 //System.out.println(stockList.get(i).getId()+","+stockList.get(i).getClose()+","+stockList.get(i).getHigh()+","+stockList.get(i).getLow()
//		 // +","+stockList.get(i).getOpen()+","+stockList.get(i).getVolume());
//		 }
		while (this.running) {
			for (Stock stock : stockList) {
				ctx.collect(stock);
				Thread.sleep(2500);// sleep for 10000ms
			}
			stockList = getStock(symbol);// for getting new refreshed data
		}
	}

	@Override
	public void cancel() {
		running = false;
	}

	/**
	 * Implements the string tokenizer that splits sentences into words as a
	 * user-defined FlatMapFunction. The function takes a line (String) and
	 * splits it into multiple pairs in the form of "(word,1)" (
	 * {@code Tuple2<String, Integer>}).
	 */
	@SuppressWarnings("serial")
	public static final class Tokenizer implements FlatMapFunction<String, Stock> {

		@Override
		public void flatMap(String value, Collector<Stock> out) throws InterruptedException {
			// normalize and split the line
			String[] tokens = value.toLowerCase().split("\\s*,\\s*");
			Stock stock = new Stock();
			System.out.println("entry-->");
			for (int i = 0; i < tokens.length; i++) {
				if (!tokens[i].equals("")) {
					if (NumberUtils.isNumber(tokens[i])) {// check if it's a
															// number
						switch (i) {
						case 1:
							stock.setClose(Double.parseDouble(tokens[i]));
							break;
						case 2:
							stock.setHigh(Double.parseDouble(tokens[i]));
							break;
						case 3:
							stock.setLow(Double.parseDouble(tokens[i]));
							break;
						case 4:
							stock.setOpen(Double.parseDouble(tokens[i]));
							break;
						case 5:
							stock.setVolume(Integer.parseInt(tokens[i]));
							break;
						}
					} else {// it's not a number now set it as id
						stock.setId(tokens[i]);
					}
				}
			}
			System.out.println("close-->" + stock.getClose());
			// queueStock.add(stock);
			// st = queueStock.poll(100, TimeUnit.MILLISECONDS);
			out.collect(stock);
		}
	}

	private static DataStream<String> getCsvDataStream(StreamExecutionEnvironment env, boolean fileOutput,
			String inputCsvPath) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(inputCsvPath);
		} else {
			// return null
			return null;
		}
	}
}
