package org.dfki.flink.stream;

import java.util.Comparator;

public class DistanceComparator implements Comparator<Double> {

	/**
	 * 
	 */

	DistanceComparator() {
	}

	/**
	 * @return -1, 1
	 * @param o1
	 * @param o2
	 *            computes the tuple result in Ascending order comparing the f2
	 *            values
	 */
	@Override
	public int compare(Double o1, Double o2) {
		if (o1 < o2) {
			return -1;
		} else if (o1 > o2) {
			return 1;
		} else {
			return 0;
		}
	}
}
