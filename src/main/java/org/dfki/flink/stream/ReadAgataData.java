package org.dfki.flink.stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

public class ReadAgataData extends RichSourceFunction<AgataData> {

	private volatile boolean isRunning = true;
	// private final StreamExecutionEnvironment env;
	// private final boolean fileOutput;
	private static String inputCsvPath;
	public boolean running = true;

	public ReadAgataData(String inputCsvPath) {
		this.inputCsvPath = inputCsvPath;
	}

	/*
	 * Returns a Stock Object that contains info about a specified stock.
	 * 
	 * @param symbol the company's stock symbol
	 * 
	 * @return a stock object containing info about the company's stock
	 * 
	 * @see Stock
	 */
	public static void main(String args[]) {

		inputCsvPath = args[0];
		getMinMaxList(getNormalData());
		
	}

	// a method to find minimum and maximum of the attribute
	public static void getMinMaxList(List<AgataData> agataDataList) {
		// the size of array is equal to the size of the column length
		Double[] minList = new Double[agataDataList.get(0).getData().length];
		Double[] maxList = new Double[agataDataList.get(0).getData().length];
		// to prevent null pointer exception
		for (AgataData agataData : agataDataList) {
			minList = agataData.getData();
			// copy done, as the variables are referenced in java, otherwise
			// they printed the same values
			maxList = Arrays.copyOf(agataData.getData(), agataData.getData().length);// this simple
																	// thing uff
			break;
		}

		for (AgataData agataData : agataDataList) {
			int i = 0;
			for (Double agData : agataData.getData()) {
				if (agData < minList[i]) {
					minList[i] = agData;
				}
				if (agData > maxList[i]) {
					maxList[i] = agData;
				}
				i++;
			}
		}
		List<Double[]> minMaxList = new ArrayList<>();
		minMaxList.add(minList);
		minMaxList.add(maxList);
		for (int i = 0; i < minList.length; i++) {
			System.out.printf("%f", minList[i]);
			System.out.print(",");
			System.out.printf("%f", maxList[i]);
			System.out.print(",");
		}
	}
	
	
	static List<AgataData> getNormalData() {
		List<AgataData> agataDataList = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputCsvPath));

			String inputStr;
			while ((inputStr = br.readLine()) != null) {
				agataDataList.add(BaseFunctions.CSVtoArrayNormal(inputStr));
			}
			br.close();
		} catch (IOException e) {
			Logger log = Logger.getLogger(ReadAgataData.class.getName());
			log.log(Level.SEVERE, e.toString(), e);
			return null;
		}
		return agataDataList;

	}

	static List<AgataData> getAgataData() {
		List<AgataData> agataDataList = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputCsvPath));

			String inputStr;
			while ((inputStr = br.readLine()) != null) {
				agataDataList.add(BaseFunctions.CSVtoArray(inputStr));
			}
			br.close();
		} catch (IOException e) {
			Logger log = Logger.getLogger(ReadAgataData.class.getName());
			log.log(Level.SEVERE, e.toString(), e);
			return null;
		}
		return agataDataList;

	}

	@Override
	public void run(SourceContext<AgataData> ctx) throws Exception {
		List<AgataData> agataDataList = getAgataData();// query agatadata
		while (this.running) {
			for (AgataData agataData : agataDataList) {
				ctx.collect(agataData);
				Thread.sleep(20);// sleep for 20ms
			}
			agataDataList = getAgataData();// for getting new refreshed data
		}
	}

	@Override
	public void cancel() {
		running = false;
	}


}
