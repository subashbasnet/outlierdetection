package org.dfki.flink.stream;

import java.io.Serializable;

public class Stock{

	// private String symbol;
	// private double price;
	// private int volume;
	// private double pe;
	// private double eps;
	// private double week52low;
	// private double week52high;
	// private double daylow;
	// private double dayhigh;
	// private double movingav50day;
	// private double marketcap;
	// private String name;
	// private String currency;
	// private double shortRatio;
	// private double previousClose;
	// private double open;
	// private String exchange;

	private String id;
	private int timeStamp;
	private double close;
	private double high;
	private double low;
	private double open;
	private int volume;

	public Stock() {
	}

	public Stock(int timeStamp, double close, double high, double low, double open, int volume, String id) {
		this.timeStamp = timeStamp;
		this.close = close;
		this.high = high;
		this.low = low;
		this.open = open;
		this.volume = volume;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public int getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(int timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

}
