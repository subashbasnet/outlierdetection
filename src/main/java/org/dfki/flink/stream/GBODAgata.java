package org.dfki.flink.stream;

import java.util.Collection;
import java.util.Iterator;

import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.contrib.streaming.DataStreamUtils;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.shaded.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.dfki.flink.stream.GBOD.Grid;
import org.dfki.flink.stream.StockAnalysis.Point;
import org.dfki.flink.stream.StockAnalysis.PointNormalized;
import org.dfki.flink.stream.StockAnalysis.getPoints;

/**
 * final time window is taken 30 seconds, and total counts = 600 manually
 * Input Agruments: /home/subash/Desktop/thesis/data/agata.csv 5
 **/
public class GBODAgata {

	public static void main(String[] args) throws Exception {
		StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStream<AgataData> edits = see.addSource(new ReadAgataData(args[0]));

		@SuppressWarnings("serial")
		KeyedStream<AgataData, String> keyedEdits = edits.keyBy(new KeySelector<AgataData, String>() {
			@Override
			public String getKey(AgataData event) {
				return event.getId();
			}
		});

		Double[] columns1 = new Double[41];
		@SuppressWarnings("serial")
		DataStream<Tuple2<String, Double[]>> newDataStream = keyedEdits.timeWindow(Time.seconds(5))
				.fold(new Tuple2<>("", columns1), new FoldFunction<AgataData, Tuple2<String, Double[]>>() {
					@Override
					public Tuple2<String, Double[]> fold(Tuple2<String, Double[]> st, AgataData value) {
						Double[] columns = new Double[41];// close,high,low,open,volume
						columns = value.getData();
						return (new Tuple2<String, Double[]>(value.getId(), columns));
					}
				});

		DataStream<Point> points = newDataStream.map(new getPoints());
		Double[] minMax = { 0.000000, 54.227570, 0.000000, 12.653610, 0.000000, 0.000000, 0.000000, 2.000000, 0.000000,
				24.000000, 4.000000, 99.000000, 0.000000, 41701.000000, 0.000000, 58900903.000000, 0.000000, 20.000000,
				0.000000, 819.000000, 228.983333, 1394.483333, 0.000000, 20.000000, 0.000000, 8.000000, 0.000000,
				2.000000, 0.000000, 41498.000000, 0.000000, 99.900000, 0.000000, 50.000000, 0.000000, 2240.000000,
				0.000000, 23.539000, 0.000000, 99.900000, 0.000000, 1.000000, 0.000000, 1.000000, 0.000000, 1565.000000,
				0.000000, 815.000000, 0.000000, 1145.000000, 0.000000, 999.000000, 0.000000, 91.229000, 0.000000,
				25.332000, 0.000000, 1.000000, 0.000000, 100.000000, 0.000000, 16.042800, 0.000000, 823.000000,
				0.000000, 580.000000, 0.000000, 54.495500, 0.000000, 15.000000, 0.000000, 45.900000, 0.000000,
				26.200000, 0.000000, 1.000000, 0.000000, 100.000000, 0.000000, 1.000000, 0.000000, 1.000000 };
		DataStream<Point> newPoints = points.map(new PointNormalized(minMax));
		DataStream<Tuple2<Point, Grid>> pointsWithGridCoordinates = newPoints
				.map(new RoundDownNewPoints(Integer.parseInt(args[1])));

		DataStream<Tuple3<Grid, Double, Long>> gridWithDensity = pointsWithGridCoordinates.map(new AddCountAppender())
				.keyBy(2).timeWindow(Time.seconds(10)).reduce(new GridPointsCount()).map(new RetrieveGridWithCount())
				.map(new CalculateGridDensity(500));
		// gridWithDensity.print();
		DataStream<Tuple4<Grid, Long, Boolean, Integer>> finalGrid = gridWithDensity.map(new FindOutlierGrid(500));
		Iterator<Tuple4<Grid, Long, Boolean, Integer>> iter2 = DataStreamUtils.collect(finalGrid);
		Collection<Tuple4<Grid, Long, Boolean, Integer>> finalGridCollection = Lists.newArrayList(iter2);
		DataStream<Tuple2<Point, Boolean>> finalPoints = pointsWithGridCoordinates
				.map(new FindOutlierPoint(finalGridCollection));
		finalPoints.print();
//		finalPoints.writeAsCsv("file:///home/subash/Desktop/test", FileSystem.WriteMode.NO_OVERWRITE, "\n", " ");
		see.execute();
	}

	/**
	 * finding the outlier points
	 **/
	public static final class FindOutlierPoint extends RichMapFunction<Tuple2<Point, Grid>, Tuple2<Point, Boolean>> {
		private Collection<Tuple4<Grid, Long, Boolean, Integer>> finalGridCollection;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public FindOutlierPoint(Collection<Tuple4<Grid, Long, Boolean, Integer>> finalGridCollection) {
			this.finalGridCollection = finalGridCollection;
		}

		@Override
		public Tuple2<Point, Boolean> map(Tuple2<Point, Grid> gd) throws Exception {

			for (Tuple4<Grid, Long, Boolean, Integer> e : finalGridCollection) {
				if (gd.f1.id.equals(e.f0.id)) {
					return new Tuple2<Point, Boolean>(gd.f0, e.f2);
				}
			}
			// this should never reach
			return new Tuple2<Point, Boolean>(gd.f0, false);
		}
	}

	/**
	 * sums the density of each point and adds it to each element
	 */
	@ForwardedFields("0")
	public static final class TotalGridDensity implements ReduceFunction<Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> reduce(Tuple4<Grid, Double, Long, Double> val1,
				Tuple4<Grid, Double, Long, Double> val2) {
			return new Tuple4<Grid, Double, Long, Double>(val1.f0, val1.f1, val1.f2, val1.f1 + val2.f1);
		}
	}

	/**
	 * maps grid coordinate points
	 */
	public static final class TotalGridDensityMap
			implements MapFunction<Tuple4<Grid, Double, Long, Double>, Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> map(Tuple4<Grid, Double, Long, Double> t) throws Exception {
			return new Tuple4<Grid, Double, Long, Double>(t.f0, t.f1, t.f2, t.f3);
		}
	}

	/** Append grid total density as 1.0 initally. */
	@ForwardedFields("f0;f1")
	public static final class AddTotalIntialDensity
			implements MapFunction<Tuple3<Grid, Double, Long>, Tuple4<Grid, Double, Long, Double>> {

		@Override
		public Tuple4<Grid, Double, Long, Double> map(Tuple3<Grid, Double, Long> t) {
			return new Tuple4<Grid, Double, Long, Double>(t.f0, t.f1, t.f2, 1D);// not
																				// sure
																				// about
																				// 1D
																				// here
																				// check??
		}
	}

	/**
	 * finding the outlier grids
	 **/
	public static final class FindOutlierGrid
			extends RichMapFunction<Tuple3<Grid, Double, Long>, Tuple4<Grid, Long, Boolean, Integer>> {
		int count = 0;
		int totalPoints = 0;
		int gbod_count_limit = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public FindOutlierGrid(int totalPoints) {
			this.totalPoints = totalPoints;
			this.count = 0;
		}

		@Override
		public Tuple4<Grid, Long, Boolean, Integer> map(Tuple3<Grid, Double, Long> gd) throws Exception {
			// Collections.sort(gridWithDensityList, new
			// GridDensityComparator());
			// here limit = (total grid density/total number of grids(p^m))
			double limit = (totalPoints) * 0.025;// GBOD considers 5% of
													// the element to be
													// outliers but
			// we couldn't implement that, so we implemented grids with density
			// or grid less than 5% of the average density
			// or total elements count is considered as outlier grids, the
			// result will be compared.
			// had to change from 5% to 2.5% to find less number of grids as
			// outlier
			if (gbod_count_limit > 500) {// the size of the elements taken
											// currently
				gbod_count_limit = 0;
				count = 0;
			}
			count++;
			gbod_count_limit += gd.f2;
			if (gd.f2 < limit) {
				if (count < 4) {// taking 4 number of elements
					return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, true, count);
				} else {
					return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, false, count);
				}
			} else {
				return new Tuple4<Grid, Long, Boolean, Integer>(gd.f0, gd.f2, false, count);
			}
		}
	}

	/**
	 * finding density of each grid
	 **/
	public static final class CalculateGridDensity
			extends RichMapFunction<Tuple2<Grid, Long>, Tuple3<Grid, Double, Long>> {

		int totalPoints = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public CalculateGridDensity(int totalPoints) {
			this.totalPoints = totalPoints;
		}

		@Override
		public Tuple3<Grid, Double, Long> map(Tuple2<Grid, Long> gd) throws Exception {

			return new Tuple3<Grid, Double, Long>(gd.f0, (double) gd.f1 / (double) totalPoints, gd.f1);
		}
	}

	/**
	 * Grid based
	 */
	public static final class RetrieveGridWithCount
			implements MapFunction<Tuple4<Point, Grid, String, Long>, Tuple2<Grid, Long>> {

		@Override
		public Tuple2<Grid, Long> map(Tuple4<Point, Grid, String, Long> t) throws Exception {
			return new Tuple2<Grid, Long>(t.f1, t.f3);
		}
	}

	/**
	 * counts the number of points in a grid
	 */
	@ForwardedFields("0")
	public static final class GridPointsCount implements ReduceFunction<Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> reduce(Tuple4<Point, Grid, String, Long> val1,
				Tuple4<Point, Grid, String, Long> val2) {
			return new Tuple4<Point, Grid, String, Long>(val1.f0, val1.f1, val1.f2, val1.f3 + val2.f3);
		}
	}

	/** Appends a count variable to the tuple. */
	@ForwardedFields("f0;f1")
	public static final class AddCountAppender
			implements MapFunction<Tuple2<Point, Grid>, Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> map(Tuple2<Point, Grid> t) {
			return new Tuple4<Point, Grid, String, Long>(t.f0, t.f1, t.f1.id, 1L);
		}
	}

	/**
	 * now let's multiply each value by p, the atrributes value will range from
	 * 0 to p. the return will be integer values so it'll correspond to the
	 * coordinate of the grid. i.e. the value says which grid the point belongs
	 * to.
	 **/
	public static final class RoundDownNewPoints extends RichMapFunction<Point, Tuple2<Point, Grid>> {

		int parameter = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public RoundDownNewPoints(int parameter) {
			this.parameter = parameter;
		}

		@Override
		public Tuple2<Point, Grid> map(Point pt) throws Exception {

			return new Tuple2<Point, Grid>(pt, BaseFunctions.getRoundDownValues(pt.pt, parameter));
		}
	}
}
