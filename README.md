* Implements Outlier Detection Algorithm for Flink using streaming functions. 

### How do I get set up? ###

* Clone the git repository to your local.
* Observe pom file to know about flink batch configurations and  dependencies.
* Maven clean build the project.
* Eg: to run GBODAgataMOdifiedData, provide input parameters of length 3. 
<file-data-input-path><p>
Eg:Input Agruments: /home/subash/Desktop/thesis/data/agata.csv 5
* The input data files can be found in data folder of the project. 
* Paste the file in your local location and adapt to the input parameters.

### Contribution guidelines ###
*Extend the codes for other outlier methods
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Subash Basnet
* Flink community